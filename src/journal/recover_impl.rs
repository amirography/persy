use crate::{
    error::PERes,
    journal::{JournalEntry, JournalId},
    persy::{CommitStatus, PersyImpl},
    transaction::tx_impl::TransactionImpl,
    RecoverStatus,
};
use std::collections::HashMap;

pub enum RecoverTransaction {
    /// Started but not completed
    Started(TransactionImpl),
    /// Successfully prepared
    PrepareCommit(TransactionImpl),
    /// rollback-ed
    Rollback(TransactionImpl),
    /// Successfully committed after prepared
    Commit(TransactionImpl),
    /// Successfully cleaned up resources after commit
    Cleanup((Vec<u8>, JournalId)),
}
impl RecoverTransaction {
    fn recover_status(&self) -> RecoverStatus {
        match self {
            Self::Started(_) => RecoverStatus::Started,
            Self::PrepareCommit(_) => RecoverStatus::PrepareCommit,
            Self::Commit(_) => RecoverStatus::Commit,
            Self::Rollback(_) => RecoverStatus::Rollback,
            Self::Cleanup(_) => RecoverStatus::Cleanup,
        }
    }
    fn meta_id(&self) -> &Vec<u8> {
        match self {
            Self::Started(tx) => tx.meta_id(),
            Self::PrepareCommit(tx) => tx.meta_id(),
            Self::Commit(tx) => tx.meta_id(),
            Self::Rollback(tx) => tx.meta_id(),
            Self::Cleanup((m, _)) => m,
        }
    }

    fn tx(&mut self) -> Option<&mut TransactionImpl> {
        match self {
            Self::Started(tx) => Some(tx),
            Self::PrepareCommit(tx) => Some(tx),
            Self::Commit(tx) => Some(tx),
            Self::Rollback(tx) => Some(tx),
            Self::Cleanup(_) => None,
        }
    }
    fn next_state(self, res: PERes<RecoverStatus>) -> Self {
        match res {
            Err(_) => match self {
                RecoverTransaction::Started(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::Rollback(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::PrepareCommit(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::Commit(s) => RecoverTransaction::Commit(s),
                RecoverTransaction::Cleanup(s) => RecoverTransaction::Cleanup(s),
            },
            Ok(RecoverStatus::Started) => match self {
                RecoverTransaction::Started(s) => RecoverTransaction::Started(s),
                RecoverTransaction::Rollback(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::PrepareCommit(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::Commit(s) => RecoverTransaction::Commit(s),
                RecoverTransaction::Cleanup(s) => RecoverTransaction::Cleanup(s),
            },
            Ok(RecoverStatus::PrepareCommit) => match self {
                RecoverTransaction::Started(s) => RecoverTransaction::PrepareCommit(s),
                RecoverTransaction::Rollback(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::PrepareCommit(s) => RecoverTransaction::PrepareCommit(s),
                RecoverTransaction::Commit(s) => RecoverTransaction::Commit(s),
                RecoverTransaction::Cleanup(s) => RecoverTransaction::Cleanup(s),
            },
            Ok(RecoverStatus::Commit) => match self {
                RecoverTransaction::Started(s) => RecoverTransaction::Commit(s),
                RecoverTransaction::Rollback(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::PrepareCommit(s) => RecoverTransaction::Commit(s),
                RecoverTransaction::Commit(s) => RecoverTransaction::Commit(s),
                RecoverTransaction::Cleanup(s) => RecoverTransaction::Cleanup(s),
            },
            Ok(RecoverStatus::Rollback) => match self {
                RecoverTransaction::Started(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::Rollback(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::PrepareCommit(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::Commit(s) => RecoverTransaction::Commit(s),
                RecoverTransaction::Cleanup(s) => RecoverTransaction::Cleanup(s),
            },
            Ok(RecoverStatus::Cleanup) => match self {
                RecoverTransaction::Started(s) => RecoverTransaction::Cleanup((s.meta_id().clone(), s.id().clone())),
                RecoverTransaction::Rollback(s) => RecoverTransaction::Rollback(s),
                RecoverTransaction::PrepareCommit(s) => {
                    RecoverTransaction::Cleanup((s.meta_id().clone(), s.id().clone()))
                }
                RecoverTransaction::Commit(s) => RecoverTransaction::Cleanup((s.meta_id().clone(), s.id().clone())),
                RecoverTransaction::Cleanup(s) => RecoverTransaction::Cleanup(s),
            },
        }
    }
}

#[derive(Default)]
pub struct RecoverImpl {
    pub(crate) tx_id: HashMap<Vec<u8>, JournalId>,
    pub(crate) transactions: HashMap<JournalId, (Option<RecoverTransaction>, Option<CommitStatus>)>,
    pub(crate) in_cleaning_reallocated: HashMap<JournalId, Vec<u64>>,
    pub(crate) order: Vec<JournalId>,
}
impl std::fmt::Debug for RecoverImpl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} {}, {:?}", self.tx_id, self.transactions.len(), self.order)
    }
}

fn new_allocation(in_cleaning: &mut HashMap<JournalId, Vec<u64>>, allocation: u64) {
    for v in in_cleaning.values_mut() {
        v.push(allocation)
    }
}

pub(crate) struct RecoverRefs<'a> {
    tx: &'a mut TransactionImpl,
    in_cleaning_reallocated: &'a mut HashMap<JournalId, Vec<u64>>,
}
impl<'a> RecoverRefs<'a> {
    pub(crate) fn tx(&mut self) -> &mut TransactionImpl {
        self.tx
    }
    pub(crate) fn new_allocation(&mut self, allocation: u64) {
        new_allocation(self.in_cleaning_reallocated, allocation)
    }
}

impl RecoverImpl {
    pub fn apply<C>(&mut self, recover: C) -> PERes<()>
    where
        C: Fn(&Vec<u8>) -> bool,
    {
        for (id, status) in self.list_transactions() {
            if status == RecoverStatus::PrepareCommit {
                if recover(&id) {
                    self.commit(id);
                } else {
                    self.rollback(id);
                }
            }
        }
        Ok(())
    }

    pub fn list_transactions(&self) -> Vec<(Vec<u8>, RecoverStatus)> {
        let mut res = Vec::new();
        for id in &self.order {
            if let Some((id, status)) = self
                .transactions
                .get(id)
                .and_then(|(s, _)| s.as_ref().map(|tx| (tx.meta_id().clone(), tx.recover_status())))
            {
                res.push((id, status));
            }
        }
        res
    }

    pub fn status(&self, tx_id: Vec<u8>) -> Option<RecoverStatus> {
        if let Some(id) = self.tx_id.get(&tx_id) {
            self.transactions
                .get(id)
                .and_then(|(t, _)| t.as_ref().map(|x| x.recover_status()))
        } else {
            None
        }
    }

    pub fn commit(&mut self, tx_id: Vec<u8>) {
        if let Some(id) = self.tx_id.get(&tx_id) {
            if let Some(tx) = self.transactions.get_mut(id) {
                tx.1 = Some(CommitStatus::Commit);
            }
        }
    }

    pub fn rollback(&mut self, tx_id: Vec<u8>) {
        if let Some(id) = self.tx_id.get(&tx_id) {
            if let Some(tx) = self.transactions.get_mut(id) {
                tx.1 = Some(CommitStatus::Rollback);
            }
        }
    }

    pub(crate) fn recover_journal_entry(&mut self, record: &dyn JournalEntry, id: &JournalId) {
        let transactions = &mut self.transactions;

        let (status, _) = transactions.entry(id.clone()).or_insert_with(|| {
            (
                Some(RecoverTransaction::Started(TransactionImpl::recover(id.clone()))),
                None,
            )
        });
        let cl = &mut self.in_cleaning_reallocated;
        let res = if let Some(status) = status {
            if let Some(tx) = status.tx() {
                let mut rec_ref = RecoverRefs {
                    tx,
                    in_cleaning_reallocated: cl,
                };
                let res = record.recover(&mut rec_ref);
                match &res {
                    Ok(RecoverStatus::PrepareCommit) => {
                        self.order.push(id.clone());
                    }
                    Ok(RecoverStatus::Commit) => {
                        self.in_cleaning_reallocated.insert(id.clone(), Vec::new());
                    }
                    Ok(RecoverStatus::Cleanup) => {
                        self.in_cleaning_reallocated.remove(id);
                    }
                    _ => {}
                }
                Some(res)
            } else {
                None
            }
        } else {
            None
        };
        if let Some(res) = res {
            *status = (*status).take().map(|s| s.next_state(res));
        }
    }

    pub(crate) fn finish_journal_read(&mut self) {
        for (id, pages) in self.in_cleaning_reallocated.drain() {
            if let Some((Some(status), _)) = self.transactions.get_mut(&id) {
                if let Some(tx) = status.tx() {
                    tx.remove_free_pages(pages);
                }
            }
        }
        for (id, (tx, _)) in &self.transactions {
            if let Some(tx) = tx {
                self.tx_id.insert(tx.meta_id().clone(), id.clone());
            }
        }
    }

    pub fn final_recover(mut self, persy: &PersyImpl) -> PERes<()> {
        let allocator = &persy.allocator();
        let journal = &persy.journal();
        let address = &persy.address();
        let snapshots = &persy.snapshots();
        for id in self.order {
            if let Some((Some(status), chosen)) = self.transactions.remove(&id) {
                match status {
                    RecoverTransaction::PrepareCommit(mut tx) => match chosen {
                        Some(CommitStatus::Commit) | None => {
                            let prepared_result = tx.recover_prepare(persy);
                            if let Ok(prepared) = prepared_result {
                                tx.recover_commit(persy, prepared)?;
                                journal.finished_to_clean(&[id])?;
                            }
                        }
                        Some(CommitStatus::Rollback) => {
                            tx.recover_rollback(persy)?;
                        }
                    },
                    RecoverTransaction::Commit(mut tx) => {
                        tx.recover_commit_cleanup(persy)?;
                        journal.finished_to_clean(&[id])?;
                    }
                    RecoverTransaction::Cleanup((_, id)) => {
                        journal.cleaned_to_trim(&[id]);
                    }
                    RecoverTransaction::Started(tx) => {
                        tx.recover_rollback(persy)?;
                    }
                    RecoverTransaction::Rollback(tx) => {
                        tx.recover_rollback(persy)?;
                    }
                }
            }
        }
        for (_, (status, _)) in self.transactions.iter_mut() {
            if let Some(status) = status {
                if let Some(tx) = status.tx() {
                    tx.recover_rollback(persy)?;
                }
            }
        }
        address.recompute_last_pages()?;
        address.flush_segments()?;
        allocator.recover_sync()?;
        journal.clear_in_queue(snapshots)?;
        allocator.trim_free_at_end()?;
        Ok(())
    }

    pub(crate) fn journal_page(&mut self, allocation: u64) {
        new_allocation(&mut self.in_cleaning_reallocated, allocation)
    }
}
